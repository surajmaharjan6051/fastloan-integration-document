`URL`
    
     /middleware/api/v1/loan/list

`Request Method`
    
    POST

`Headers`

| SN  |Field | Data Type |                     Description                  | 
|:---:|:----:|:---------:|:------------------------------------------------:|
|  1  | partnerCode  |    String    |  Client Id of the business, provided by FastLoan.|

`Payload`

| SN  |Field | Data Type |                   Description                    | 
|:---:|:----:|:---------:|:------------------------------------------------:|
|  1  | searchParam  |    String    |      Enum ( ACCOUNT_CODE,UNIQUE_IDENTIFIER)      |
|  1  | searchParamValue  |    String    |     Customer account no or unique identifier     |
|  1  | status  |    String    | Loan Status that needs to be fetched. ACTIVE, COMPLETED, ALL, REJECTED|